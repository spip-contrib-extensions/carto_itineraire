<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'carto_itineraire_description' => 'Ce plugin contient un modèle pour afficher un calcul d’itinéraire dans une carte sur une page',
	'carto_itineraire_nom' => 'Modèle calcul itinéraire',
	'carto_itineraire_slogan' => 'Un modèle pour afficher un calcul d’itinéraire dans une page',
);
